﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BasicEnemyScript : MonoBehaviour
{
    private Vector3 rockSpeed;
    [SerializeField]
    //speed in positive y direction
    float speed;
    [SerializeField]
    MainCharacterController controller_values;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3((int)Random.Range(controller_values.leftMost - controller_values.startColumn,1 + controller_values.rightMost - controller_values.startColumn),-11,-30);
        rockSpeed = new Vector3(0,speed, 0);
    }

    // Update is called once per frame
    void Update()
    {
        transform.position += rockSpeed*Time.deltaTime;

        if (transform.position.y > 5)
        {
            Destroy(gameObject);
        }
    }

    public void setSpeed(float new_speed)
    {
        speed = new_speed;
    }
}
