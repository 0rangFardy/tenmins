﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class GameOverScript : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.anyKeyDown)
        {
            if (Input.GetKey("space"))
            {
                //execute some code
                //
                ResetEverything();
                //gameObject.SetActive(false);
            }
        }
    }

    void ResetEverything()
    {
        /*
        GameObject[] all_enemies = GameObject.FindGameObjectsWithTag("Enemy");
        foreach (GameObject enemy in all_enemies)
        {
            Destroy(enemy);
            
        }
        StatsTrack.resetHighScore();
        */
        SceneManager.LoadScene("SampleScene");
    }
}
