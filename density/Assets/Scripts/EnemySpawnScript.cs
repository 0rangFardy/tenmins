﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemySpawnScript : MonoBehaviour
{

    [SerializeField] BasicEnemyScript basic_enemy;
    [SerializeField] PenguinEnemyScript penguin_enemy;
    public int[] Stages;

    float speed;
    int current_level;


    // Start is called before the first frame update
    void Start()
    {
        current_level = 1;
        StartCoroutine(RandomEnemySpawn());
    }

    private void Update()
    {
        if (StatsTrack.getHighScore() > Stages[0] && current_level == 1)
        {
            //start coroutine
            StartCoroutine(PenguinSpawn());
            current_level++;
        }
    }

    IEnumerator RandomEnemySpawn()
    {
        while (true)
        {
            BasicEnemyScript enemy =  Instantiate(basic_enemy);
            //Debug.Log("Enemy Spawned");

            //yield return new WaitForSeconds(100f*Time.deltaTime);
            yield return new WaitForSeconds(Random.Range(0.5f,2.0f));
        }
    }

    IEnumerator PenguinSpawn()
    {
        while (true)
        {
            PenguinEnemyScript penguin = Instantiate(penguin_enemy);
            yield return new WaitForSeconds(Random.Range(0.5f, 2f));
        }
        
    }
}
