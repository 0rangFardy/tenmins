﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BackEnvironMove : MonoBehaviour
{
    private Vector3 velocity;
    public float speed;

    // Update is called once per frame
    void FixedUpdate()
    {
        velocity = new Vector3(0f, speed * Time.deltaTime, 0f);
        transform.position += velocity;

        if (transform.position.y >= 15)
        {
            transform.position = new Vector3(0, -15, -1);
        }
    }
}
