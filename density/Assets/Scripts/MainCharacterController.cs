﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MainCharacterController : MonoBehaviour
{
    private int column;

    //specifies the rows that the player and enemies can be in
    public int playerMovement;
    public int maximum;
    public GameObject gameOver;
    public int leftMost;
    public int rightMost;
    public int startColumn;

    // Start is called before the first frame update
    void Start()
    {

        column = startColumn;

    }

    // Update is called once per frame
    void Update()
    {
        GetInput();
        if (playerMovement != 0)
        {
            if (column + playerMovement >= leftMost && column + playerMovement <= rightMost)
            {
                transform.position = new Vector3(transform.position.x + playerMovement, transform.position.y, transform.position.z);
                column += playerMovement;
            }
        }
    }

    private void GetInput()
    {
        if (Input.anyKeyDown)
        {
            playerMovement = (int)Mathf.Clamp(Input.GetAxisRaw("Horizontal") - Input.GetAxisRaw("Vertical"),-1f,1f);
            
        }
        else
        {
            playerMovement = 0;
        }
    }

    private void OnCollisionEnter2D(Collision2D collision)
    {
        Debug.Log("COLLIDE"); 
        if (collision.transform.tag == "Enemy")
        {
            Destroy(gameObject);
            gameOver.SetActive(true);
        }
    }
}
