﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PenguinEnemyScript : MonoBehaviour
{
    private Vector3 rockSpeed;
    [SerializeField]
    float speed;
    [SerializeField]
    MainCharacterController controller_values;
    [SerializeField]
    Vector3 offset;

    // Start is called before the first frame update
    void Start()
    {
        transform.position = new Vector3((int)Random.Range(controller_values.leftMost - controller_values.startColumn, 1 + controller_values.rightMost - controller_values.startColumn), -11, -30);
        rockSpeed = new Vector3(0, speed, 0);


        RaycastHit2D hit = Physics2D.Raycast(transform.position + offset, rockSpeed);
        if (hit.collider != null && hit.collider.tag == "Enemy")
        {
            Debug.Log("Destroy penguin");
            Destroy(gameObject);
        }

    }

    // Update is called once per frame
    void Update()
    {



        transform.position += rockSpeed * Time.deltaTime;

        if (transform.position.y > 5)
        {
            Destroy(gameObject);
        }
    }

    public void setSpeed(float new_speed)
    {
        speed = new_speed;
    }
}
