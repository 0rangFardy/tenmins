﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatsTrack : MonoBehaviour
{
    [SerializeField] private static float highScore;
    public static StatsTrack instance;
    public float rate;

    // Start is called before the first frame update
    void Start()
    {
        instance = this;
        highScore = 0;
        rate = 2f;
    }

    // Update is called once per frame
    void Update()
    {
        highScore += Time.deltaTime * rate;
    }


    static public float getHighScore()
    {
        return highScore; 
    }

    static public void resetHighScore()
    {
        highScore = 0f;
    }

}
